from flask import Blueprint, render_template

from app.classes.StarShips import StarShips

starships_blueprint = Blueprint('starships_blueprint', __name__)

@starships_blueprint.route(rule='/', methods=['GET'])
def index():
    response, prevPage, nextPage = StarShips().getAll()
    return render_template("starships.html",starships=response, prevPage=prevPage, nextPage=nextPage)