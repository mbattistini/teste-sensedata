import re

from flask import Blueprint, render_template, json, request, redirect

from app.classes.Filtros import Filtros
from app.classes.personagem import Personagem

personagem_blueprint = Blueprint('personagem_blueprint', __name__)

def makeUrl(page):
    if page is not None:
        print(request.base_url)
        return request.url_root+'/personagem/page/'+re.sub('[^0-9]', '', page)
    return None


@personagem_blueprint.route(rule='/', methods=['GET'])
def main():
    response, pages = Personagem().getAll()
    results = response
    atualPage = 1
    if atualPage < pages:
        nextPage = atualPage =+ 1
        nextPage = '/personagem/name/'+str(nextPage)
    else:
        nextPage = None
    prevPage = None
    films = Filtros().getFilm()
    starships = Filtros().getStartship()
    vehicles = Filtros().getVehicles()
    planets = Filtros().getPlanets()
    url = request.url
    return render_template("personagem.html",nextPage=nextPage, prevPage=prevPage, atualPage=atualPage, url=url,
                           results=results, films=films, starships=starships, vehicles=vehicles, planets=planets)

@personagem_blueprint.route(rule='/<ordem>', methods=['GET'])
@personagem_blueprint.route(rule='/<ordem>/<page>', methods=['GET'])
def getPage(ordem='name', page='1'):
    atualPage = int(page)
    response, pages = Personagem().getAll(order=ordem, page=page)
    results = response
    if atualPage < pages:
        nextPage = atualPage + 1
        nextPage = '/personagem/'+ordem+'/'+str(nextPage)
    else:
        nextPage = None
    if atualPage > 1:
        prevPage = atualPage - 1
        prevPage = '/personagem/'+ordem+'/' + str(prevPage)
    else:
        prevPage = None

    url = request.url
    return render_template("personagem.html", nextPage=nextPage, prevPage=prevPage, atualPage=atualPage, url=url,
                           results=results)


@personagem_blueprint.route(rule='/filtrar', methods=['POST'])
def filtrar():
    page = request.form.get('page')
    order = request.form.get('order')
    films = request.form.get('films')
    starships = request.form.get('starships')
    vehicles = request.form.get('vehicles')
    planets = request.form.get('planets')
    url = request.form.get('url')
    response, pages = Personagem().getAll(order=order, page=page,films=films)
    print(url)
    return redirect(location=url)
