from flask import Blueprint, redirect, render_template

main_blueprint = Blueprint('main_blueprint', __name__)

@main_blueprint.route(rule='/', methods=['GET'])
def main():
    return render_template("index.html")

