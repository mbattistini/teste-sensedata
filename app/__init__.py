import os

from flask import Flask
from app.blueprints.main import main_blueprint
from app.blueprints.personagem import personagem_blueprint
from app.blueprints.starships import starships_blueprint


def create_app(config_filename):
    static_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'static').replace('/app/', '/')
    tmpl_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates').replace('/app/', '/')

    app = Flask(__name__, template_folder=tmpl_dir, static_folder=static_dir, static_url_path='/static')

    app.config.from_object(config_filename)



    app.register_blueprint(main_blueprint)
    app.register_blueprint(personagem_blueprint, url_prefix="/personagem")
    app.register_blueprint(starships_blueprint, url_prefix="/starships")

    return app
