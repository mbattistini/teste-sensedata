import requests
from flask import json

from config import RESOURCES

class Personagem(object):
    def __init__(self):
        self.url = RESOURCES["people"]

    def __get_name(self,personagem):
        return personagem.get('name')

    def __get_gender(self, personagem):
        return personagem.get('gender')

    def __get_mass(self, personagem):
        return personagem.get('mass')

    def __get_height(self, personagem):
        return personagem.get('height')

    def __get_dados(self):
        result = []
        url = self.url
        while True:
            response = requests.get(url=url)
            if response.status_code != 200:
                break
            response = json.loads(response.text)
            rows = response['results']
            for row in rows:
                result.append(row)
            url = response['next']
            if url is None:
                break
        return result



    def getAll(self,order='name',page=1,**filters):
        response = self.__get_dados()
        if order == 'name':
            response.sort(key=self.__get_name)
        elif order == 'gender':
            response.sort(key=self.__get_gender)
        elif order == 'mass':
            response.sort(key=self.__get_mass, reverse=True)
        elif order == 'height':
            response.sort(key=self.__get_height, reverse=True)
        else:
            response.sort(key=self.__get_name)

        films = filters.get('film')
        starships = filters.get('starships')
        vehicles = filters.get('vehicles')
        planets = filters.get('planets')

        # rows = response
        # response = []
        # for row in rows:
        #     print(row)

        response = [response[i:i+10] for i in range(0, len(response), 10)]
        pages =len(response)
        response = response[int(page)]
        return response, pages

