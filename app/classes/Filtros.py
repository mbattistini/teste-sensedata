import requests
from flask import json

from config import RESOURCES


class Filtros(object):

    def __requestData(self,url):
        response = requests.get(url=url)
        if response.status_code == 200:
            response = json.loads(response.text)
            return response
        else:
            return False

    def getFilm(self):
        result = []
        response = self.__requestData(url=RESOURCES["films"])
        response = response['results']
        for row in response:
            data = dict()
            data["title"] = row["title"]
            result.append(data)
        return result

    def getStartship(self):
        result = []
        response = self.__requestData(url=RESOURCES["starships"])
        response = response['results']
        for row in response:
            data = dict()
            data["name"] = row["name"]
            result.append(data)
        return result

    def getVehicles(self):
        result = []
        response = self.__requestData(url=RESOURCES["vehicles"])
        response = response['results']
        for row in response:
            data = dict()
            data["name"] = row["name"]
            result.append(data)
        return result

    def getPlanets(self):
        result = []
        response = self.__requestData(url=RESOURCES["planets"])
        response = response['results']
        for row in response:
            data = dict()
            data["name"] = row["name"]
            result.append(data)
        return result
