import requests
from flask import json

from config import RESOURCES


class StarShips(object):

    def __get_score(self,row):
        return row.get('score')

    def __get_dados(self, url):
        result = []
        while True:
            response = requests.get(url=url)
            if response.status_code != 200:
                break
            response = json.loads(response.text)
            rows = response['results']
            for row in rows:
                result.append(row)
            url = response['next']
            if url is None:
                break
        return result

    def getAll(self):
        response = self.__get_dados(url=RESOURCES['starships'])
        rows = []
        for row in response:
            try:
                hyperdrive_rating = float(row['hyperdrive_rating'])
            except:
                hyperdrive_rating = 0
            try:
                cost_in_credits = float(row['cost_in_credits'])
            except:
                cost_in_credits = 0

            score = (hyperdrive_rating / cost_in_credits) if cost_in_credits != 0 else 0.0
            row['score'] = score
            rows.append(row )

        rows.sort(key=self.__get_score, reverse=True)
        response = rows
        return response, None, None
